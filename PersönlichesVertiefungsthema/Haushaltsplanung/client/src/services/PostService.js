import axios from 'axios';

// const url = 'http://localhost:5000/api/posts/';
const url = 'http://raspberrypi:5000/api/posts/';

class PostService {
    // Get Posts
    static getSyncPosts() { // Just for learning issues
        const response = axios.get(url).then(res => {
            const data = res.data;
            return data.map(post => (
            {
                ...post,
                createdAt: new Date(post.createdAt)
            }));
        }).catch(e => console.log(e));
        return response;
    }

    static async getPosts() {
        try
        {
            const res = await axios.get(url);
            const data = res.data;
            return data.map(post => (
            {
                ...post,
                createdAt: new Date(post.createdAt)
            })); 
        }
        catch(e)
        {
            console.log(e.message);
        }
    }

    // Create Posts
    static insertPost(text) {
        return axios.post(url, {text: text});
    }

    // Delete Posts
    static deletePost(id) {
        return axios.delete(`${url}${id}`);
    }
}

export default PostService;