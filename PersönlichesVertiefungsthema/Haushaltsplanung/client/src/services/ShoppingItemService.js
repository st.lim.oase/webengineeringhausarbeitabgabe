import axios from 'axios';

// const url = 'http://localhost:5000/api/shoppingList/';
const url = 'http://raspberrypi:5000/api/shoppingList/';

class ItemService {
    // Get Items
    static getSyncItems() { // Just for learning issues
        const response = axios.get(url).then(res => {
            const data = res.data;
            return data.map(item => (
            {
                ...item,
                createdAt: new Date(item.createdAt)
            }));
        }).catch(e => console.log(e));
        return response;
    }

    static async getItems() {
        try
        {
            const res = await axios.get(url);
            const data = res.data;
            return data.map(item => (
            {
                ...item,
                createdAt: new Date(item.createdAt)
            })); 
        }
        catch(e)
        {
            console.log(e.message);
        }
    }

    // Create Items
    static insertItem(item) {
        return axios.post(url, item);
    }

    // Delete Items
    static deleteItem(id) {
        return axios.delete(`${url}${id}`);
    }

    // Update Items
    static updateItem(item) {
        return axios.put(`${url}${item._id}`, item);
    }
}

export default ItemService;