import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

// Componten importieren
import ShoppingList from '../views/ShoppingList'
import Shoppings from '../views/Shoppings'
import ShoppingGraphics from '../views/ShoppingGraphics'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/shoppingList',
    name: 'ShoppingList',
    component: ShoppingList
  },
  {
    path: '/shoppings',
    name: 'Shoppings',
    component: Shoppings
  },
  {
    path: '/shoppingGraphics',
    name: 'ShoppingGraphics',
    component: ShoppingGraphics
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
