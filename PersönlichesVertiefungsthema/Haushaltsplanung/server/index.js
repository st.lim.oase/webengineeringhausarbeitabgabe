const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

//Middleware
app.use(bodyParser.json());
app.use(cors());

const posts = require('./routes/api/posts');
const shoppings = require('./routes/api/shoppings');
const shoppingList = require('./routes/api/shoppingList');

app.use('/api/posts', posts);
app.use('/api/shoppings', shoppings);
app.use('/api/shoppingList', shoppingList);

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Sever started on port ${port}`))