const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get Shoppings
router.get('/', async (req, res) => {
    const shoppings = await loadShoppingCollection();
    res.send(await shoppings.find({}).toArray());
});

// Add Shoppings
router.post('/', async (req, res) => {
    const shoppings = await loadShoppingCollection();
    await shoppings.insertOne({
        product: req.body.product,
        category: req.body.category,
        price: req.body.price,
        createdAt: req.body.createdAt
    });

    res.status(201).send();
});


// Delete Shoppings
router.delete('/:id', async (req, res) => {
    const shoppings = await loadShoppingCollection();
    await shoppings.deleteOne({_id: new mongodb.ObjectID(req.params.id)});

    res.status(200).send();
});

async function loadShoppingCollection() 
{
    return getClient('shoppings');    
}

getClient = async (collection) =>
{
    const client = await mongodb.MongoClient.connect('mongodb+srv://userDummy:passwordDummy@domainDummy-j0ppl.mongodb.net/test?retryWrites=true&w=majority', 
        { useNewUrlParser: true, useUnifiedTopology: true });

    return client.db('oase').collection(collection);
}
module.exports = router;

// db-user: userDummy
// db-pass: passwordDummy