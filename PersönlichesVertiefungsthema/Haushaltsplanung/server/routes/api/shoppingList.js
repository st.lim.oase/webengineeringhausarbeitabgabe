const express = require('express');
const mongodb = require('mongodb');

const router = express.Router();

// Get Shoppings
router.get('/', async (req, res) => {
    const shoppings = await loadShoppingCollection();
    res.send(await shoppings.find({}).toArray());
});

// Add Shoppings
router.post('/', async (req, res) => {
    const shoppings = await loadShoppingCollection();
    await shoppings.insertOne({
        product: req.body.product,
        completed: req.body.completed,
        createdAt: new Date()
    });

    res.status(201).send();
});


// Delete Shoppings
router.delete('/:id', async (req, res) => {
    const shoppings = await loadShoppingCollection();
    await shoppings.deleteOne({_id: new mongodb.ObjectID(req.params.id)});

    res.status(200).send();
});

// Update Shopping
router.put('/:id', async (req, res) => {
    try{
        console.log(req.params.id + " " + req.body.product + ", " + req.body.completed);
        const shoppings = await loadShoppingCollection();
        await shoppings.updateOne(
        {
            _id: new mongodb.ObjectID(req.params.id)
        },
        {
            $set:
            {
                product: req.body.product,
                completed: req.body.completed,
            }
        });
        res.status(200).send();
    }
    catch(e)
    {
        res.status(500).send(e.message);
    }
            
});

async function loadShoppingCollection() 
{
    return getClient('shoppingList');
}

getClient = async (collection) =>
{
    const client = await mongodb.MongoClient.connect('mongodb+srv://userDummy:passwordDummy@domainDummy-j0ppl.mongodb.net/test?retryWrites=true&w=majority', 
        { useNewUrlParser: true, useUnifiedTopology: true });

    return client.db('oase').collection(collection);
}

module.exports = router;

// db-user: userDummy
// db-pass: passwordDummy