import Menuvertical from './Menu/MenuV.js';
import Menuhorizontal from './Menu/MenuH.js';

component = {
    template: `<div class="container">
                    <div class="header column">
                        <h2>WWW-Navigator</h2>    
                        <ul>{{ topbuttons }}</ul>
                    </div>
                    <div class="menu column"><ul>{{ sidebuttons }}</ul></div>
                    <div class="main column">{{ mainText }}</div>
                    <div class="aside column">{{ asideText }}</div>
                    <div class="footer column">Dieses Programm dient dazu verschiedene Funktionalitäten von HTML, CSS und JavaScript für den Anwender leicht zugänglich zu machen.</div>
                </div>`,
    data: function()
    {
        return {top};
    },
    created: async () => {
        const response = await Promise.all([fetch(uri1)]);                
        const content = JSON.parse(await Promise.all([response[0].text()]));         
        push([], "","", `StateManagement.html`);
    
        createMenuButtons(content);
    },
    computed: {
        topbuttons() {return "hello world top";},
        sidebuttons() {return "hello world side";},
        mainText() {return "maintext";},
        //asideText() {return "asidetext";}
    }
}

const routes = [{ path: '/', component: component}];

const router = new VueRouter({
    routes
});

const store = new Vuex.Store({
    state: {
        data: [], 
        mainText : "", 
        asideText: ""
    },
    mutations
});

const fetchData = async () => 
{
    
}

var vue = new Vue({
    el: '#app',
    components: {
        router,
    }
});
