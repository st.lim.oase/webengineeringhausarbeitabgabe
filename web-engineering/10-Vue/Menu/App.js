import Menuvertical from './MenuV.js';
import Menuhorizontal from './MenuH.js';

new Vue({
    el: '#app',
    components: {
        Menuvertical,
        Menuhorizontal
    }
});