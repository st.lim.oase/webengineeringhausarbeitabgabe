export default {
    template: `
        <div :class=alignment>         
            <link rel="stylesheet" type="text/css" href="./Menu.css" media="screen"></link>
            <ul>
                <li><div class="button">1</div></li>
                <li><div class="button">1</div></li>
                <li><div class="button">1</div></li>
                <li><div class="button">1</div></li>
                <li><div class="button">1</div></li>
                <li><div class="button">1</div></li>
            </ul>
        </div>`,
    data: () => { return {
        count: 0,
        alignment: "vertical",
    }},
    methods: { 
        increment() { this.count++ },
    }
}

