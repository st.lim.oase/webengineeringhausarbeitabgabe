export default {
    template: `<div>
            <button @click="increment">You clicked me {{ count }} times</button>
        </div>`,
    data: () => { return {
        count: 0
    }},
    methods: { increment() { this.count++ } }
}
