console.log("Aufgabe2");
var COUNT = 1;
///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function pubsub()
{
    var subscribers = [];
    return {
        subscribe : function (func)
        {
            subscribers.push(func);
        },
        publish : function (x)
        {
            subscribers.forEach(element => {
                element(x);
            });
        }
    }
}

my_pubsub = pubsub();
my_pubsub.subscribe((x) => console.log("1: " + x));
my_pubsub.subscribe((x) => console.log("2: " + x));
my_pubsub.subscribe((x) => console.log("3: " + x));
my_pubsub.publish("runs");
my_pubsub.subscribe((x) => console.log("4: " + x));
my_pubsub.publish("a new one");

///////////////////////////////
console.log("Teil " + COUNT++ + ":");
function gensymf(x)
{
    var counter = 0;
    return function()
    {
        console.log(x + counter);
        return x + counter++;
    }
}
gensym = gensymf('G');
gensym();
gensym();
gensym();
gensym();
gensym();
gensym();

///////////////////////////////
console.log("Teil " + COUNT++ + ":");
function fibonacci(x, y)
{
    var counter = 0;
    var firstElem = x;
    var secondElem = y;
    return function()
    {
        counter++;
        if(counter === 1)
            return firstElem;
        if(counter === 2)
            return secondElem;

        let j = firstElem;
        let z = secondElem;
        
        for(let i = 2; i < counter; i++)
        {
            tempz = z;
            z = z + j;
            j = tempz;
        }
        return z;
    }
}

fib = fibonacci(0,1);
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());

///////////////////////////////
console.log("Teil " + COUNT++ + ":");
function addg(x)
{
    return function(y)
    {
        if(y != null)
        {
            return addg(x+y);
        }
        else{
            return x;
        }
    }
}

console.log(addg(3)(4)(5)(6)(7)());
console.log(addg(1)(2)(4)(8)());

///////////////////////////////
console.log("Teil " + COUNT++ + ":");

add = (x, y) => x+y;
mul = (x, y) => x*y;

console.log(typeof add)

function applyg(func, x)
{
    return function(y)
    {
        if(y != null)
            if(typeof x === "number")
                return applyg(func, func(x, y));
            else 
                return applyg(func, y);

        else
            return x;
    }
}

console.log(applyg(add)(3)(4)(5)());
console.log(applyg(mul)(2)(4)(8)());

///////////////////////////////
console.log("Teil " + COUNT++ + ":");
function m(val, src)
{
    console.log("create m with:" + val + ", " + src);
    var obj = {
        value : val,
        source : val.toString()

    }

    if(src === undefined)
        obj.source = val.toString();
    else 
        obj.source = src;
    return obj;
}
var m1 = m(1);
var mpi = m(Math.PI, "pi");
console.log(JSON.stringify(m1)); // {"value": 1, "source": "1"}
console.log(JSON.stringify(mpi)); // {"value": 3.14159..., "source": "pi"} 


///////////////////////////////
console.log("Teil " + COUNT++ + ":");
function addm(m1, m2)
{
    return m(m1.value + m2.value, "(" + m1.value + "+" + m2.value + ")");
}

console.log(JSON.stringify(addm(m(3), m(4))));

///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function binarymf(func, sign)
{    
    return function(m1, m2)
    {
        return m(func(m1.value, m2.value), "(" + m1.value + sign + m2.value + ")");        
    }
}
addmandm = binarymf(add, "+");
console.log(JSON.stringify(addmandm(m(3), m(4))));

///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function binarymf2(func, sign)
{    
    return function(m1, m2)
    {
        let m1obj = typeof m1 === "number" ? m(m1) : m1;        
        let m2obj = typeof m2 === "number" ? m(m2) : m2;
        return m(func(m1obj.value, m2obj.value), "(" + m1obj.value + sign + m2obj.value + ")");
    }
}
addmandm2 = binarymf2(add, "+");
console.log(JSON.stringify(addmandm2(3, 4)));

///////////////////////////////
console.log("Teil " + COUNT++ + ":");

square = (x) => x*x;

function unarymf(func, text)
{
    return function(x)
    {
        return m(func(x), "("+text+" "+x+")");
    }
}
squarem = unarymf(square, "square");
console.log(JSON.stringify(squarem(4)));

///////////////////////////////
console.log("Teil " + COUNT++ + ":");

hyp = (a, b) => Math.sqrt(a*a+b*b);
console.log(hyp(3, 4));

///////////////////////////////
console.log("Teil " + COUNT++ + ":");
function exp(arr)
{
    console.log(arr);
    if(typeof arr === "number")
        return arr;    

    if(arr.length > 0)
    {  
        if(typeof arr[0] === "function")
            var func = arr[0];
        else
            return 0;
        if(arr.length == 2)
            return func(exp(arr[1]));
        if(arr.length == 3)
            return func(exp(arr[1]), exp(arr[2]));
        if(arr.length == 4)
            return func(exp(arr[1]), exp(arr[2]), exp(arr[3]));
    }    
}
hypa = [ Math.sqrt, [ add, [mul, 3, 3], [mul, 4, 4] ] ];
console.log(exp(hypa));

///////////////////////////////
console.log("Teil " + COUNT++ + ":");

var variable;
store = (x) => variable = x;

store(5);
console.log(variable === 5);

///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function quatre(func, val1, val2, end)
{
    end(func(val1, val2));
}

identityf = (x) => x;

quatre( add, identityf(3), identityf(4), store ); // variable === 7 
console.log(variable === 7)

///////////////////////////////
console.log("Teil " + COUNT++ + ":");
function unaryc(func)
{
    return (n, end) => end(func(n));
}
sqrtc = unaryc(Math.sqrt); 
sqrtc(81, store); // variable === 9 
console.log(variable === 9);


///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function binaryc2(func)
{
    return (n1, n2, end) => end(func(n1, n2));
}

addc = binaryc2(add); addc(4, 5, store);
console.log(variable === 9);
mulc = binaryc2(mul); mulc(2, 3, store);
console.log(variable === 6);
