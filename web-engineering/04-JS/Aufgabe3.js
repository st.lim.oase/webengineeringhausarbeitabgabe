console.log("Aufgabe3");
var COUNT = 1;
///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function fifo()
{
    var arr = [];
    return {
        push : function(element)
        {
            arr.push(element);
            this.print();
        },
        pop : function()
        {
            if(this.size() > 0)
            {
                var result = arr.shift();
                this.print();
                return result;
            }
            else
                return null;
        },
        size : () => arr.length,
        print : () => console.log(arr),
    }
}

var fifoList = fifo();
fifoList.push(1);
fifoList.push(2);
fifoList.push(3);
fifoList.push(4);
fifoList.pop();
fifoList.pop();
fifoList.push(5);
fifoList.push(6);
fifoList.pop();
fifoList.pop();
fifoList.pop();
fifoList.pop();
fifoList.pop();
fifoList.pop();
fifoList.pop();
fifoList.pop();
fifoList.pop();
fifoList.pop();


///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function unorderedSet()
{
    var arr = [];
    return {
        push : function(element)
        {
            if(this.contains(element))
                return;
            arr.push(element);
            this.print();
        },
        pop : function()
        {
            if(this.size() > 0)
            {
                var result = arr.pop();
                this.print();
                return result;
            }
            else
                return null;
        },
        size : () => arr.length,
        print : () => console.log(arr),
        contains : function(toCheck)
        {
            var found = false;
            arr.forEach(element => {
                if(element === toCheck)
                    found = true;
            });
            return found;
        }
    }
}

var unordered = unorderedSet();
unordered.push(1);
unordered.push(2);
unordered.push(3);
unordered.push(4);
unordered.push(4);
unordered.push(4);
unordered.push(4);
unordered.push(4);
unordered.push(4);
unordered.push(5);
unordered.push(5);
unordered.push(5);
unordered.push(5);
unordered.push(5);
unordered.push(5);
///////////////////////////////
console.log("Teil " + COUNT++ + ":");

function unorderedMultiSet()
{
    var arr = [];
    return {
        push : function(element)
        {
            arr.push(element);
            this.print();
        },
        pop : function()
        {
            if(this.size() > 0)
            {
                var result = arr.pop();
                this.print();
                return result;
            }
            else
                return null;
        },
        size : () => arr.length,
        print : () => console.log(arr),
    }
}

var unorderedMulti = unorderedMultiSet();
unorderedMulti.push(1);
unorderedMulti.push(2);
unorderedMulti.push(3);
unorderedMulti.push(4);
unorderedMulti.push(4);
unorderedMulti.push(4);
unorderedMulti.push(4);