//////////////////////////////////////
console.log("Teil 1:");
function identity_function(a)
{
 return function ()
 {
  return a;
 }
}
console.log(identity_function(5)());
//////////////////////////////////////
console.log("Teil 2:");
function addf(x)
{
    return function(y)
    {
        return x + y;
    }
}
console.log(addf(5)(3));
//////////////////////////////////////
console.log("Teil 3:");
function addf(x)
{
    return function(y)
    {
        return x + y;
    }
}

function mul(x)
{
    return function(y)
    {
        return x * y;
    }
}

function applyf(func)
{
    return func;
}
console.log(applyf(addf)(3)(4));
//////////////////////////////////////
console.log("Teil 4:");
function curry(func, arg)
{
    return function(arg2)
    {
        return func(arg)(arg2);
    }
}
console.log(curry(addf, 3)(4));
//////////////////////////////////////
console.log("Teil 5:");
inc1 = addf(1);
console.log(inc1(4));
inc2 = applyf(addf)(1);
console.log(inc2(5));
inc3 = curry(addf, 1);
console.log(inc3(6));
//////////////////////////////////////
console.log("Teil 6:");
function methodize(func)
{
    return function(b)
    {
        return func(this)(b);
    }
}
Number.prototype.add = methodize(addf);
console.log((3).add(4));
//////////////////////////////////////
console.log("Teil 7:");
function demethodize(func)
{
    return function(a, b)
    {        
        return func.call(a, b);
    }
}
console.log(
    demethodize(Number.prototype.add)(3,5)
);
//////////////////////////////////////
console.log("Teil 8:");
function mul(x, y)
{
    return x * y;
}
function add(x,y)
{
    return x + y;
}
function twice(func)
{
    return function(x)
    {
        return func(x, x);
    }
}
double = twice(add);
d = double(11);
console.log(d);
square = twice(mul);
s = square(11);
console.log(s);

//////////////////////////////////////
console.log("Teil 8:");
function composeu(func1, func2)
{
    return function (op)
    {
        return func2(func1(op));
    }
}
console.log(composeu(double, square)(3));

//////////////////////////////////////
console.log("Teil 9:");
function composeb(func1, func2)
{
    return function(a, b, c)
    {
        return func2(func1(a, b), c)
    }
}
console.log(composeb(add, mul)(2, 3, 5));

//////////////////////////////////////
console.log("Teil 10:");
function once(func)
{
    var counter = 0;
    return function(a, b)
    {
        console.log("counter: " + counter)
        if(counter != 0)     
        {
            counter++;            
            console.log("Error!");
        }
        else
        {   
            counter++;
            return func(a, b);
        }
    }
}
addonce = once(add);
console.log(addonce(3,4));
console.log(addonce(3,4));

//////////////////////////////////////
console.log("Teil 11:");
function counterf(x)
{    
    return {
        inc : function()
        {
            return x+1;
        },
        dec : function()
        {
            return x-1;
        }
    }
}
counter = counterf(10);
console.log(counter.inc());
console.log(counter.dec());

//////////////////////////////////////
console.log("Teil 12:");
function revocable(func)
{
    var isRevoced = false;
    return {
        invoke : function(x)
        {
            if(!isRevoced) {
                func(x);
                return "Success";
            }
            else
                return "Feherlabbruch";
        },
        revoke : function()
        {
            isRevoced = true;
        }
    }
}
temp = revocable(console.log);
console.log(temp.invoke(7)); // führt zu alert(7);
console.log(temp.invoke(9)); // führt zu alert(9);
temp.revoke();
console.log(temp.invoke(8)); // Fehlerabbruch!

//////////////////////////////////////
console.log("Teil 13:");
class ArrayWrapper {
    constructor(_array)
    {
        let array = _array;
        this.getArray = () => array;
        this.storeArray = (input) => array = input;
        this.appendElement = (element) => array.push(element);
    }
}

wrapper = new ArrayWrapper([1, 2, 3]);
console.log(wrapper.getArray());
wrapper.storeArray([2,3,4]);
console.log(wrapper.getArray());
wrapper.appendElement(5);
console.log(wrapper.getArray());
console.log(wrapper.array);