( () => {
    const component = {
        name: 'counter',
        ccm: 'https://ccmjs.github.io/ccm/ccm.js',
        config: {
            counter: 0,
            current: 0
        },
        Instance: function() {
            
            this.update = () => 
            {
                this.counter++; 
                this.start();
            }

            inc = () => {this.current++; this.update();};
            dec = () => {this.current--; this.update();};

            var buttonup = document.createElement("button");
            buttonup.innerText = "Counter +";
            buttonup.onclick = inc;
            var buttondown = document.createElement("button");
            buttondown.innerText = "Counter -";
            buttondown.onclick = dec;

            
            this.start = async () => {            
                this.element.innerHTML = `<div id="container">
                                            <div>
                                                Clicked: <span id="counter">${this.counter}</span>
                                            </div>
                                            <div>
                                                Current: <span id="current">${this.current}</span>
                                            </div>
                                        </div>`;
                this.element.querySelector("#container").appendChild(buttonup);
                this.element.querySelector("#container").appendChild(buttondown);
            }
        }
    };
    let b="ccm."+component.name+(component.version?"-"+component.version.join("."):"")+".js";if(window.ccm&&null===window.ccm.files[b])return window.ccm.files[b]=component;(b=window.ccm&&window.ccm.components[component.name])&&b.ccm&&(component.ccm=b.ccm);"string"===typeof component.ccm&&(component.ccm={url:component.ccm});let c=(component.ccm.url.match(/(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)/)||["latest"])[0];if(window.ccm&&window.ccm[c])window.ccm[c].component(component);else{var a=document.createElement("script");document.head.appendChild(a);component.ccm.integrity&&a.setAttribute("integrity",component.ccm.integrity);component.ccm.crossorigin&&a.setAttribute("crossorigin",component.ccm.crossorigin);a.onload=function(){window.ccm[c].component(component);document.head.removeChild(a)};a.src=component.ccm.url}
})();