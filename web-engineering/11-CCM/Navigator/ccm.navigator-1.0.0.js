( () => {
    const component = {
        name: 'navigator',
        ccm: 'https://ccmjs.github.io/ccm/ccm.js',
        config: {
            html: [ 'ccm.load' , './navigatortemplate.html' ],
            url: ""
        },
        Instance: function() {
            this.start = async () => {    
                const $ = this.ccm.helper;
                $.setContent ( this.element, $.html( this.html, {
                }));
            
                const headerlist = this.element.querySelector('div.header ul');
                const menulist = this.element.querySelector('div.menu ul');
                const textField = this.element.querySelector('div.main');
                const aside = this.element.querySelector('div.aside');

                
                const fetchData = async () => 
                {
                    const response = await Promise.all([fetch(this.url)]);                
                    const content = JSON.parse(await Promise.all([response[0].text()]));            
                    createMenuButtons(content);
                }

                const createMenuButtons = (data) => {
                    data.forEach(element => {
                        createListItem(headerlist, element.toptheme, () => 
                        {
                            textField.innerHTML = element.description;
                            aside.innerHTML = `<a href="${element.url}" target="_blank">Weiter führende Links</a>`;
                            
                            createNavButtons(element.subthemes);
                        });
                    });
                }

                const removeNavButtons = () => {
                    var child = menulist.firstElementChild;
                    while (child)
                    {
                        menulist.removeChild(child);
                        child = menulist.firstElementChild;
                    }
                }

                const createNavButtons = (data) => {
                    removeNavButtons();
                    data.forEach(element => {
                        createListItem(menulist, element.subtheme, () =>
                        {
                            textField.innerHTML = element.description;
                            aside.innerHTML = `<a href="${element.url}" target="_blank">Weiter führende Links</a>`;
                        });
                    });
                }

                const createListItem = (list, theme, func) => 
                {
                    const listItem = document.createElement('li');
                    list.appendChild(listItem);
                    listItem.innerHTML = `<div class="button">${theme}</div>`;
                    const deleteButton = listItem.querySelector('div');
                    deleteButton.onclick = () => func();
                }

                console.log("fetchdata: " + this.url);
                fetchData();
            }
        }
    };
    let b="ccm."+component.name+(component.version?"-"+component.version.join("."):"")+".js";if(window.ccm&&null===window.ccm.files[b])return window.ccm.files[b]=component;(b=window.ccm&&window.ccm.components[component.name])&&b.ccm&&(component.ccm=b.ccm);"string"===typeof component.ccm&&(component.ccm={url:component.ccm});let c=(component.ccm.url.match(/(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)/)||["latest"])[0];if(window.ccm&&window.ccm[c])window.ccm[c].component(component);else{var a=document.createElement("script");document.head.appendChild(a);component.ccm.integrity&&a.setAttribute("integrity",component.ccm.integrity);component.ccm.crossorigin&&a.setAttribute("crossorigin",component.ccm.crossorigin);a.onload=function(){window.ccm[c].component(component);document.head.removeChild(a)};a.src=component.ccm.url}
})();


